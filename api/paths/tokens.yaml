definitions:
  Token:
    description: Token
    type: object
    properties:
      id:
        type: string
        description: Token Id
        example: "tid-f3694710-ec08-4352-b8bc-b094ae40a0f4"
      name:
        type: string
        description: A label/display name for the the token
        example: "Token For Automation"
      accessToken:
        type: string
        description: The access token that can be used for authentication
        example: "yeRpaRKtJwYJMglFAouYmpoRuN8syAmWXW75ca57AdO"
      identifier:
        type: string
        description: Owner of the token. Typically the user id
        example: "uid-743e4cee-448e-4f82-b880-8a530265b1e5"
      clientId:
        type: string
        description: Client ID for whom the token has been issued
        example: "cid-sdk"
      expires:
        type: integer
        description: Time (epoch) when the token expires
        example: 1725615073184
      lastUsedAt:
        type: string
        description: Time when token was last used
        example: 2023-09-07T13:18:35.000Z
      scope:
        type: object
        description: Token permissions. This is a map of routes with r (read) or rw (read write) permission
        example:
          '*': 'rw'

parameters:
  tokenId:
    name: tokenId
    in: path
    description: Token Id
    required: true
    schema:
      type: string

/tokens:
  get:
    operationId: getTokens
    summary: List Tokens
    description: List the API tokens created by the user. Note that tokens are personal (per user) and not global.
    tags: [ "Tokens" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '../parameters.yaml#/PaginationPage'
      - $ref: '../parameters.yaml#/PaginationPerPage'
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                tokens:
                  type: array
                  items:
                    $ref: '#/definitions/Token'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/tokens"

  post:
    operationId: createToken
    summary: Create Token
    description: Create an API token
    tags: [ "Tokens" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              name:
                $ref: '#/definitions/Token/properties/name'
              expiresAt:
                $ref: '#/definitions/Token/properties/expires'
              scope:
                $ref: '#/definitions/Token/properties/scope'
            required:
              - name
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              $ref: '#/definitions/Token'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/tokens" --data '{"name":"Token For Automation"}'

/tokens/{tokenId}:
  get:
      operationId: getToken
      summary: Get Token
      description: Get token by ID
      tags: [ "Tokens" ]
      security:
        - bearer_auth: [ 'read' ]
        - query_auth: [ 'read' ]
      parameters:
        - $ref: '#/parameters/tokenId'
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/definitions/Token'
        401:
          $ref: '../responses.yaml#/unauthorized'
        403:
          $ref: '../responses.yaml#/forbidden'
        404:
          $ref: '../responses.yaml#/not_found'
        500:
          $ref: '../responses.yaml#/server_error'
      x-codeSamples:
        - lang: cURL
          source: |-
            curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/tokens/$TOKEN_ID"

  delete:
    operationId: deleteToken
    summary: Delete Token
    description: Deletes the Token. All future requests using this token will instantly fail.
    tags: [ "Tokens" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/tokenId'
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X DELETE -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/tokens/$TOKEN_ID"
