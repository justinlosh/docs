definitions:
  ProfileConfig:
    type: object
    properties:
      lockUserProfiles:
        type: boolean
        description: If profile changes by users is disabled
        example: true
      mandatory2FA:
        type: boolean
        description: If two factor authentication for all users is enforced
        example: true
    required:
      - lockUserProfiles
      - mandatory2FA

/user_directory/profile_config:
  get:
    operationId: getUserDirectoryProfileConfig
    summary: Get profile config
    description: Get the current profile configuration for all users in the user directory.
    tags: [ "User Directory" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              $ref: '#/definitions/ProfileConfig'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/user_directory/profile_config"

  post:
    operationId: setUserDirectoryProfileConfig
    summary: Set profile config
    description: Set the current profile configuration for all users in the user directory.
    tags: [ "User Directory" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/definitions/ProfileConfig'
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/user_directory/profile_config" --data '{ "lockUserProfiles": false, "mandatory2FA": false }'
