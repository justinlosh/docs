# <img src="/img/changedetection-logo.png" width="25px"> Change Detection App

## About

Change detection is the best and simplest self-hosted free open source website change detection monitoring and notification service.

* Questions? Ask in the [Cloudron Forum - Change Detection](https://forum.cloudron.io/category/149/change-detection)
* [Change Detection Website](https://changedetection.io/)
* [Change Detection issue tracker](https://github.com/dgtlmoon/changedetection.io/issues)


## Notifications

Change Detection uses [AppRise](https://github.com/caronc/apprise) to send notifications.

An example email notification using SMTP:

```
mailtos://mail.example.com?to=<to-email>&user=<username>&pass=<password>&from=<from-email>
```

