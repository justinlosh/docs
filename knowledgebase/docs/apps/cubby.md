# <img src="/img/cubby-logo.png" width="25px"> Cubby App

## About

Cubby is a filesharing app with built-in viewers. It further supports an external collabora office installation.

* Questions? Ask in the [Cloudron Forum - Cubby](https://forum.cloudron.io/category/132/cubby)
* [Cubby Website](https://getcubby.org/)
* [Cubby issue tracker](https://github.com/cloudron-io/cubby/issues)

## Admin

The first user is made Admin. This admin user can make other users admins.

Admins can:

* Manage users
* Configure Groups & Group Folders
* Configure Office Integration

## Sharing

Cubby supports three types of sharing:

* [Sharing with internal users](#internal-share)
* [Sharing with external users](#external-share)
* [Group Folders](#group-folders)

### Internal Share

Files and Folders can be shared with other users using the 'Internal Share' mechanism. An Internal Share may be
revoked at any time.

Note that Internal Shares become unavailable when the owner of the file/folder is deleted from Cubby.

### External Share

Files and Folders can be shared with external users (users who do not have an account) via the 'External Share'
mechanism. An External Share can be revoked at any time.

Note that External Shares become unavailable when the owner of the file/folder is deleted from Cubby.

### Group Folders

Group Folders are directories shared by a group of users. Group Folders do not have an owner and all users
have equal permissions. Contents of Group Folders are tracked separately and not part of any specific
user's data. This means that contents of Group Folders are still available if a user is removed entirely.


