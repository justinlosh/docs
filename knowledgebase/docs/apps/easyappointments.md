# <img src="/img/easyappointments-logo.png" width="25px"> Easy!Appointments App

## About

Easy!Appointments is a web appointment scheduler.

* Questions? Ask in the [Cloudron Forum - EasyAppointments](https://forum.cloudron.io/category/123/easy-appointments)
* [EasyAppointments Website](https://easyappointments.org)
* [EasyAppointments issue tracker](https://github.com/alextselegidis/easyappointments/issues)
