# <img src="/img/filepizza-logo.png" width="25px"> File Pizza App

## About

File Pizza imlements peer-to-peer file transfers in your browser.

* Questions? Ask in the [Cloudron Forum - File Pizza](https://forum.cloudron.io/category/66/filepizza)
* [File Pizza Website](http://file.pizza)
* [Upstream File Pizza issue tracker](https://github.com/kern/filepizza/issues)

