# <img src="/img/gogs-logo.png" width="25px"> Gogs App

## About

Gogs is a painless self-hosted Git service.

* Questions? Ask in the [Cloudron Forum - Gogs](https://forum.cloudron.io/category/70/gogs)
* [Gogs Website](https://gogs.io)
* [Gogs issue tracker](https://github.com/gogs/gogs/issues)

## Customizing Gogs

Gogs supports various [customizations](https://gogs.io/docs/installation/configuration_and_run).
To add customizations, use the [File Manager](/apps#file-manager)) and
edit the file named `/app/data/app.ini`.

After editing, restart the app for the changes to take effect.

## Custom Templates

You can override HTML templates (including templates for emails) by creating a customized version under
`/app/data/custom/templates/` directory. See [Custom Templates](https://gogs.io/docs/features/custom_template) 
for more information.

You can edit the files using the [File Manager](/apps/#file-manager). After editing,
restart the app for changes to take effect.

## CLI

The gogs CLI can be used as follows:

```
sudo -u git /home/git/gogs/gogs -c /run/gogs/app.ini
```

For example, to delete inactive accounts:

```
sudo -u git /home/git/gogs/gogs admin delete-inactive-users -c /run/gogs/app.ini
```

To create a backup:

```
sudo -u git GOGS_CUSTOM=/app/data/custom /home/git/gogs/gogs backup -c /run/gogs/app.ini --target /app/data/backups
```

