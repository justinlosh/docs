# <img src="/img/invoiceninja-logo.png" width="25px"> Invoice Ninja 5 App

## About

InvoiceNinja 5 is the leading self-host platform to create invoices, accept payments, track expenses & time tasks. Support WePay, Stripe, Braintree, PayPal, Zapier, and more!

* Questions? Ask in the [Cloudron Forum - Invoice Ninja](https://forum.cloudron.io/category/11/invoice-ninja)
* [Invoice Ninja Website](https://www.invoiceninja.org)
* [Invoice Ninja forum](https://forum.invoiceninja.com/)

## Changing Domain

Invoice Ninja stores the domain for each company dataset. This is saved when the company is first setup and cannot be changed later from the UI.
If the domain for the app instance is adjusted in Cloudron, those database records need to be updated manually.

For instances which only use **one domain** for all company datasets run the following mysql command in a [web terminal](/apps#web-terminal):
```
mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "UPDATE companies SET portal_domain='${CLOUDRON_APP_ORIGIN}'"
```

Otherwise adjust the SQL command above to only update the domains which need updating.

The client portal URL may also need to be updated. This can be done from the Invoice Ninja internal settings page under the `Client Portal` section.

## Customizations

InvoiceNinja [customizations](https://invoiceninja.github.io/en/env-variables/) can be made by opening a [File Manager](/apps#file-manager)
and editing `/app/data/env`.

