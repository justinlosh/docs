# <img src="/img/kavita-logo.png" width="25px"> Kavita App

## About

Kavita is a fast, feature rich, cross platform reading server. Built with the goal of being a full solution for all your reading needs.

* Questions? Ask in the [Cloudron Forum - Kavita](https://forum.cloudron.io/category/186/kavita)
* [Kavita Website](https://www.kavitareader.com/)
* [Kavita issue tracker](https://github.com/Kareadita/Kavita/issues)
* [Kavita Discussions](https://github.com/Kareadita/Kavita/discussions)
* [Kavita Wiki](https://wiki.kavitareader.com/)

## Passwords

The maximum password length for Kavita is [32 characters](https://github.com/Kareadita/Kavita/issues/2877).

