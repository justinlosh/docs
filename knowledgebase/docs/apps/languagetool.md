# <img src="/img/languagetool-logo.png" width="25px"> Language Tool App

## About

LanguageTool’s multilingual grammar, style, and spell checker is used by millions of people around the world

* Questions? Ask in the [Cloudron Forum - Language Tool](https://forum.cloudron.io/category/157/languagetool)
* [Language Tool Website](https://languagetool.org/)
* [Language Tool issue tracker](https://github.com/languagetool-org/languagetool/issues)

## Security

By default, the app's API is public. To avoid misuse, it's best to change the URL to some secret. To do so:

* Edit `/app/data/env` using the [File manager](/apps/#file-manager)

* Change `API_PATH_PREFIX` to some secret. You can generate a secret using `pwgen -1s 6` .

* Restart the app.

In the apps, use `https://languagetool.domain.com/<secret>/v2` as the URL instead of the default `https://languagetool.domain.com/v2`.

To test, if `hushhush` is the secret:

```
curl -d "text=Hello world" -d "language=de-DE" https://languagetool.domain.com/hushhush/v2/check
```

!!! note "Use alphanumerals for the secret"
    The secret is part of the URL. For this reason, keep it simple and use only alphanumerals for the secret.

## n-grams

LanguageTool can optionally make use of large [n-gram data sets](https://dev.languagetool.org/finding-errors-using-n-gram-data)
to detect errors with words that are often confused, like *their* and *there*.

The n-gram data set is huge and thus not part of the LT download. The install path is the first argument for the download command below. The default is `/app/data/ngrams` but you can use e.g. a [volume](/volumes) path if you don't want ngrams to be part of your backup.

To download the n-gram data set of a language:

* Open a [Web Terminal](/apps#web-terminal) and run the command below. Replace the argument `/app/data/ngrams` with your desired path (e.g. a [volume](/volumes)) if you don't want ngrams to be part of your backup .

```
# /app/pkg/install-ngrams.sh /app/data/ngrams en es
==> Installing en ngram dataset from https://languagetool.org/download/ngram-data/ngrams-en-20150817.zip
/tmp/en.zip 100%[================================================================>]   8.35G   112MB/s    in 77s     
==> Unpacking en ngram dataset
Archive:  /tmp/en.zip
   creating: en/
   creating: en/3grams/
  inflating: en/3grams/_1e4.fdt      
  inflating: en/3grams/_1e4.si       
 extracting: en/3grams/_1e4.nvd      

...

  inflating: en/1grams/_1p.si        
==> en ngram dataset has been installed.
==> Done


Set NGRAM_DATASET_PATH=/app/data/ngrams in /app/data/env and restart the app.
```

* Open `/app/data/env` using the file manager and check that `NGRAM_DATASET_PATH` is set to the correct path.

* Restart the app.

!!! warning "Large data set"
    n-gram data sets are large. For example, the `en` is around 14GB and `de` is 3GB.

