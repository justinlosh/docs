# <img src="/img/linkwarden-logo.png" width="25px"> Linkwarden App

## About

Linkwarden is a self-hosted, open-source collaborative bookmark manager to collect, organize and archive webpages.

* Questions? Ask in the [Cloudron Forum - Linkwarden](https://forum.cloudron.io/category/195/linkwarden)
* [Linkwarden Website](https://linkwarden.app/)
* [Linkwarden GitHub](https://github.com/linkwarden/linkwarden)
* [Linkwarden issue tracker](https://github.com/linkwarden/linkwarden/issues)
