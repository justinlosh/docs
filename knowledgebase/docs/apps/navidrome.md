# <img src="/img/navidrome-logo.png" width="25px"> Navidrome App

## About

Navidrome is an open source web-based music collection server and streamer.

* Questions? Ask in the [Cloudron Forum - Navidrome](https://forum.cloudron.io/category/108/navidrome)
* [Navidrome Website](https://www.navidrome.org/)
* [Navidrome community](https://www.navidrome.org/community/)
* [Navidrome issue tracker](https://github.com/deluan/navidrome/issues)

## Music folder

To change the music folder, edit `config.toml` using the [File Manager](/apps/#file-manager)

```
MusicFolder = '/media/MyMusic`
```

Be sure to restart the app after changing the music folder location.

## Custom Configuration

Custom configuration can be placed in `/app/data/config.toml`.
See the [Navidrome docs](https://www.navidrome.org/docs/usage/configuration-options/) for
all the options.

## CLI

To trigger a scan, open a [Web terminal](/apps#web-terminal) and run the following command:

```
gosu cloudron:cloudron /app/code/navidrome -c /app/data/config.toml scan -f
```
