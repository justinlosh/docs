# <img src="/img/openwebcalendar-logo.png" width="25px"> Open Web Calendar App

## About

The Open Web Calendar uses ICS/ICal calendars online and displays them in one calendar. You can use it with Nextcloud, Outlook, Google Calendar, Meetup and other calendar systems using the ICS standard.

* Questions? Ask in the [Cloudron Forum - Open Web Calendar](https://forum.cloudron.io/category/180/open-web-calendar)
* [Open Web Calendar repo](https://github.com/niccokunzmann/open-web-calendar/)
* [Open Web Calendar issue tracker](https://github.com/niccokunzmann/open-web-calendar/issues)
