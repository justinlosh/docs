# <img src="/img/phabricator-logo.png" width="25px"> Phabricator App

!!! warning "Discontinued"
    Please note this app is not available anymore since upstream development has stopped.

## About

Phabricator is a set of tools that help companies build better software, faster.

* Questions? Ask in the [Cloudron Forum - Phabricator](https://forum.cloudron.io/category/42/phabricator)
* [Phabricator Website](http://phabricator.org/)

## Empower

A registered user can be made an administrator by running the following command in
the [Web Terminal](/apps#web-terminal):

```
# /app/code/phabricator/bin/user empower --user <username>
```

See the phabricator [docs](https://secure.phabricator.com/book/phabricator/article/unlocking/) for
more information.

## Admin recovery

When not using Cloudron authentication, If you accidentally log yourself out before adding an Auth provider, you
must use the CLI tool to recover it (or simply re-install phabricator). See [T8282](https://secure.phabricator.com/T8282) for more information.

```
# /app/code/phabricator/bin/auth recover <admin-username>
```

## Uploading large files

This app is configured to accept files upto 512MB. Note that large files need to be
dragged and dropped (instead of the file upload button).

See [Q216](https://secure.phabricator.com/Q216)

