# <img src="/img/pocketbase-logo.png" width="25px"> PocketBase App

## About

PocketBase is an Open Source backend for your next SaaS and Mobile app in 1 file.

* Questions? Ask in the [Cloudron Forum - PocketBase](https://forum.cloudron.io/category/189/pocketbase)
* [PocketBase Site](https://pocketbase.io/)
* [PocketBase Docs](https://pocketbase.io/docs)
* [PocketBase Discussions](https://github.com/pocketbase/pocketbase/discussions)
* [PocketBase issue tracker](https://github.com/pocketbase/pocketbase/issues)

## Default Site

PocketBase serves the static content in `/app/data/pb_public` (html, css, images) as the website.
By default, the Cloudron package provides an `index.html` that redirects to the admin page. You
can customize this as needed using the [File manager](/apps/#file-manager).

## Routes

There are 3 routes:

* `/` - the default site served from `/app/data/pb_public`
* `/_/` - Admin dashboard UI
* `/api/` - REST API

