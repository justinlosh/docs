# <img src="/img/shiori-logo.png" width="25px"> Shiori App

## About

Shiori is a simple bookmarks manager written in the Go language. Intended as a simple clone of Pocket.

* Questions? Ask in the [Cloudron Forum - Shiori](https://forum.cloudron.io/category/176/shiori)
* [Shiori Website](https://github.com/go-shiori/shiori)
* [Shiori Issues](https://github.com/baptisteArno/typebot.io/issues)
* [Shiori Forum](https://github.com/go-shiori/shiori/discussions)

