# <img src="/img/vaultwarden-logo.png" width="25px"> Vaultwarden App

## About

Bitwarden is an Open Source Password Management solution for individuals, teams, and business organizations.
Vaultwarden is an unofficial Bitwarden compatible server written in Rust, fully compatible with the client apps.

* Questions? Ask in the [Cloudron Forum - Vaultwarden](https://forum.cloudron.io/category/64/bitwardenrs)
* [Vaultwarden Website](https://github.com/dani-garcia/vaultwarden)
* [Vaultwarden issue tracker](https://github.com/dani-garcia/vaultwarden/issues)

## Users

Bitwarden does not support Single Sign On. This is by design for security reasons. You must create a new password for your Bitwarden account.

By default, open registration is enabled. This can be changed via the config variables by editing `/app/data/config.json` using
the [File Manager](/apps/#file-manager). For example, to disable signup but allow invitations set the variables as below:

```
  "signups_allowed": false,
  "invitations_allowed": true,
```

## Admin

The admin UI is located `/admin`. To login, look for the `admin_token` field inside `/app/data/config.json` using the [File manager](/apps/#file-manager).

<center>
<img src="/img/bitwarden-admin.png" class="shadow" width="500px">
</center>

Starting with version 1.28, [it is safer](https://github.com/dani-garcia/vaultwarden/wiki/Enabling-admin-page) to generate an admin token using the built-in hash feature. Otherwise, warnings might be shown in the logs - `Please generate a secure Argon2 PHC string by using vaultwarden hash or argon2`.

To fix, open a [web terminal](https://forum.cloudron.io/apps/#web-terminal) and run:

```
# /app/code/vaultwarden hash
Generate an Argon2id PHC string using the 'bitwarden' preset:

Password:
Confirm Password: 

ADMIN_TOKEN='$argon2id$v=19$m=65540,t=3,p=4$RCpl3a+FItyn4KBJVAtZ+EyP9+fK0hoRqqo9jEdyRJE$d7UfKfZYsZJad6OIKpzPtO2o2ccLkrHjEi5jXdWWkO0'

```

Take the above `ADMIN_TOKEN` and put it in `/app/data/config.json` in the field `admin_token`.

**Important:**

* Remove the single quotes around the argon2id string above.
* The token to login to the admin page is the password you entered above to generate the `ADMIN_TOKEN`.
* `config.json` should be edited like below (please be careful about the quoting):

```
  "admin_token": "$argon2id$v=19$m=65540,t=3,p=4$RCpl3a+FItyn4KBJVAtZ+EyP9+fK0hoRqqo9jEdyRJE$d7UfKfZYsZJad6OIKpzPtO2o2ccLkrHjEi5jXdWWkO0"
```

Restart the app and verify if token actually changed.


## Custom config

Custom environment variables can be set in `/app/data/env.sh` using the [File Manager](/apps/#file-manager).

Note that Vaultwarden's admin page generates `config.json` which overrides the above env vars. See [config docs](https://github.com/dani-garcia/vaultwarden/wiki/Configuration-overview) for more information on which values are readonly and can only be set using environment variables.

