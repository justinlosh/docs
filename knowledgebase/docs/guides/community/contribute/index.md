# Contribute to Community Guides

## Overview
This guide explains how to contribute to the community guides.

## Getting Started
To get started contributing, please review the following steps provided below. 

1. If you do not already have one, please obtain a [Cloudron Git Account](https://git.cloudron.io/users/sign_up)
2. Fork the ```cloudron/docs`` repository by [clicking here](https://git.cloudron.io/cloudron/docs/-/forks/new)
3. Consider using the pre-made "Community Guide Template", which is located [here](https://git.cloudron.io/justinlosh/cloudron-community-guides/-/blob/main/community-guide-template.md?ref_type=heads)
4. Once you have finished your additions or edits, please submit a [merge request](https://git.cloudron.io/cloudron/docs)

## Further Resources

* [Markdown Reference Guide](https://daringfireball.net/projects/markdown/basics)
* [Material for MkDocs Reference Guide](https://squidfunk.github.io/mkdocs-material/reference/)
* Supported MKDocs Extensions
    * [admonition](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)
    * [footnotes](https://squidfunk.github.io/mkdocs-material/reference/footnotes/)

## Community Guide Template
To get started, you can find a pre-made Community Guide Template located [here](https://git.cloudron.io/justinlosh/cloudron-community-guides/-/blob/main/community-guide-template.md?ref_type=heads).