# Cloudron Community Guides

!!! warning "Community Guide"
    This guide was contributed by the Cloudron Community, and does not imply support, warranty. You should utilize them at your own risk. You should be familiar with technical tasks, ensure you have backups, and throughly test.

## Overview
Welcome to the Cloudron Community Guides section! The purpose of this section is to provide [Cloudron Community](https://forum.cloudron.io) created documentation in an effort to provide additional information, how to guides, apps, and tutorials on the Cloudron platform.

## Contributing
If you are interested in contributing, please review our [how-to guide](/community/contribute).

# Community Guide Categories
<div class="grid cards" markdown>

- :fontawesome-brands-html5: __Apps__ for content and structure
- :fontawesome-icons-envelope: __Email__ for interactivity
- :fontawesome-brands-css3: __Backups__ for text running out of boxes

</div>