# Connect to MongoDB Addon

## Overview

Apps on Cloudron use an internal MongoDB database (addon). For security reasons, the database is only accessible
from inside the server and cannot be accessed directly from outside. Instead, the database is accessed using
an SSH tunnel to the server.

In this guide, we will see how to connect to the MongoDB addon database from outside (say, your laptop/PC).

## Database credentials

To get the database credentials, open the [Web terminal](/apps/#web-terminal) of an app and
run `env | grep CLOUDRON_MONGODB`. Make a note of the credentials. Note that each app has it's own separate
database and thus it's own database credentials.

<center>
<img src="/guides/img/mongodb-addon-env.png" class="shadow" width="500px">
</center>

## Internal IP Address

The internal IP address of the MongoDB server is `172.18.30.3`.

## DB Clients

### CLI

To connect via MongoDB Shell , open a [Web Terminal](https://docs.cloudron.io/apps/#web-terminal) and click
the `MongoDB` button at the top. This will paste the CLI connection command into the Web Terminal. Press enter to execute
the command and use the CLI.

### MongoDB Compass

MongoDB Compass is intuitive and flexible, provides detailed schema visualizations, real-time performance metrics, sophisticated querying abilities, and much more. It can be downloaded [here](https://www.mongodb.com/try/download/compass).

Create a new connection and click on `Advanced Connection Options`. Fill in the server and authentication options from the step above:

<center>
<img src="/guides/img/compass-mongodb-general.png" class="shadow" width="500px">
</center>

!!! note "Direct Connection"
    It is important to check the `Direct Connection` checkbox. Without this, the connection won't work.

<center>
<img src="/guides/img/compass-mongodb-auth.png" class="shadow" width="500px">
</center>

Configure SSH tunnel by clicking on the Proxy/SSH tab:

<center>
<img src="/guides/img/compass-mongodb-ssh-tunnel.png" class="shadow" width="500px">
</center>

You should now be able to view the database:

<center>
<img src="/guides/img/compass-mongodb-collections.png" class="shadow" width="500px">
</center>

### Studio 3T

[Studio 3T](https://studio3t.com/) is the Ultimate GUI for MongoDB. You can download it [here](https://studio3t.com/download/).

Create a new MongoDB connection in Studio 3T via `File` -> `Connect`.

Choose manually enter values and fill up the values gathered in the steps above:

<center>
<img src="/guides/img/studio3t-mongodb-server.png" class="shadow" width="500px">
</center>

<center>
<img src="/guides/img/studio3t-mongodb-auth.png" class="shadow" width="500px">
</center>


Configure SSH tunnel by clicking on the SSH tab:

<center>
<img src="/guides/img/studio3t-mongodb-ssh-tunnel.png" class="shadow" width="500px">
</center>

You should now be able to view the database:

<center>
<img src="/guides/img/studio3t-mongodb-collections.png" class="shadow" width="500px">
</center>

