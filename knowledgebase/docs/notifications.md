# Notifications

## Overview

Cloudron will display notifications in the dashboard about various events like:

* If app goes down
* If app run out of memory
* Low disk space
* Updates available
* App updated

The notifications can be read by clicking on the icon in the navigation bar.

<center>
<img src="/img/notifications-icon.png" class="shadow" width="500px">
</center>

Notifications can be read in the notifications page.

<center>
<img src="/img/notifications-view.png" class="shadow" width="500px">
</center>

## Health check monitor

You will have to setup a 3rd party service like [Cloud Watch](https://aws.amazon.com/cloudwatch/) or
[UptimeRobot](http://uptimerobot.com/) to monitor the Cloudron itself. You can use
`https://my.<domain>/api/v1/cloudron/status` as the health check URL.

