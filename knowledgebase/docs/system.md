# System

The System view gives an overview of the Server, CPU, Memory & Disk usage.

## Info

<center>
<img src="../img/system-info.png" class="shadow" width="500px">
</center>

## Logs

The `Logs` button can be used to view the System logs. In the situation that the dashboard is unreachable/down, the raw logs are located at `/home/yellowtent/platformdata/logs/box.log`. Up to 10MB of active logs is kept along side 5 rotated logs. Logs older than 14 days are removed.

<center>
<img src="../img/system-logs.png" class="shadow" width="500px">
</center>

## CPU Usage

<center>
<img src="../img/system-cpu.png" class="shadow" width="500px">
</center>

## Memory Usage

<center>
<img src="../img/system-memory.png" class="shadow" width="500px">
</center>

## Disk Usage

Disk usage is only computed on demand. Clicking on the `Refresh` button
in the top right will compute disk usage in that instant.

<center>
<img src="../img/system-disk-usage.png" class="shadow" width="500px">
</center>

