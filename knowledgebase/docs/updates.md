# Updates

## Cloudron Updates

Cloudron checks [cloudron.io](https://cloudron.io) periodically and applies any platform updates
automatically.

Cloudron always takes a complete backup of the platform data and all apps before applying
an update. Should the backup creation fail, the update will not be performed.

Updates are [GPG signed](/security/#updates) for security.

It is always recommended to review the official [Cloudron Changelog](https://git.cloudron.io/cloudron/box/-/blob/master/CHANGES) for a list of changes made in each version.

## App updates

Cloudron Apps are installed from the [Cloudron.io App Store](https://www.cloudron.io/store/index.html). Cloudron checks the App Store
for updates periodically and updates them based on the update settings.

Cloudron always takes a backup of an app before performing an update. Should the backup creation fail,
the update will not be applied. If an update fails or the updated app misbehaves, the
Cloudron administrator can [rollback from a backup](/backups/#restoring-an-app-from-existing-backup).

Updates can be disabled on a per app level from the `Updates` section:

<center>
<img src="/img/updates-disable-app-update.png" class="shadow" width="500px">
</center>

When app updates are disabled or an update is pending, an update indicator is shown:

<center>
<img src="/img/app_update.png" class="shadow" width="600px">
</center>

## Update schedule

The update schedule can be set in the `Updates` section in the `Settings` menu:

<center>
<img src="/img/app-update-interval.png" class="shadow">
</center>

To disable updates entirely, select `Update manually`. When automatic updates are disabled, the Cloudron
administrators will get an email notification as and when updates are available. Updates can then be
applied by clicking the update button.

## Rollback

To rollback an app update, simply [restore from a backup](/backups/#restoring-an-app).

To rollback a Cloudron update, it can be [restored from the backup](/backups/#restoring-cloudron).

## Canceled Subscription

When you cancel the subscription, the Cloudron and installed apps stop receiving updates.
The server and the apps will continue to run forever. The subscription may be renewed at
any time (we may contact you if we notice that you regularly cancel and renew your subscription).

